angular.module('workTimeTrackerApp').factory('activities', ['flipClock', '$rootScope', '$interval', function(flipClock, $rootScope, $interval) {
  var activities = [],
      intervalPromise;

  // console.log("hello!!!!"); // runs only on start

  var Activity = function(name, color, duration) {
    this.name = name;
    this.color = color;
    this.duration = duration;
  };
/*
  if (localStorage.getItem('activities')) {
    console.log("getting data from localStorage:")
    var storedAct = JSON.parse(localStorage.getItem('activities'));
    for (var i=0; i < storedAct.length; i++) {
      activities.push(new Activity(storedAct[i].name, storedAct[i].color, storedAct[i].duration));
    }
    console.log(activities);
  }
  */
  var content = fs.readFileSync(__dirname +'/test.json', "utf-8");
  processFile();

  function processFile() {
    console.log(content);
    if (content == "") {
      console.log("empty");
      activities.push(new Activity('Working',     'default',  0));
      activities.push(new Activity('Eating',      'primary',  0));
      activities.push(new Activity('Rest',        'info',     0));
      activities.push(new Activity('Web surfing', 'success',  0));
      activities.push(new Activity('Off-topic',   'warning',  0));
      activities.push(new Activity('Consulting',  'danger',   0));
    }
    else {
      var storedAct = JSON.parse(content);
      console.log(storedAct);
      for (var i=0; i < storedAct.length; i++) {
        activities.push(new Activity(storedAct[i].name, storedAct[i].color, storedAct[i].duration));
      }
    }
  }

  activities.getSumOfDurations = function() {
    return this.reduce(function(mem, act) {
      return mem + act.duration;
    }, 0);
  };

// transform this into a string

  Activity.prototype.getDurationInPct = function() {
    console.log("getDurationInPct");
    return (this.duration / activities.getSumOfDurations()) * 100;
  };

  return {
    testFunction: function() {
      console.log("yayyyyy");
    },
    getAll: function() {
      activities
        .filter(function(activity) { return !activity.name; })
        .forEach(this.remove);

      return activities;
    },

    remove: function(activity) {
      var index = activities.indexOf(activity);
      if (index > -1) {
          activities.splice(index, 1);
      }
    },

    addNew: function(name, color) {
      activities.push(new Activity(name, color));
    },

    setActive: function(activity) {
      $rootScope.currentActivity = activity;

      flipClock.restart(0);

      if (intervalPromise) {
        $interval.cancel(intervalPromise);
        intervalPromise = null;
      }

      intervalPromise = $interval(function() {
        activity.duration += 1;
      }, 1000);
    }
  };
}]);
