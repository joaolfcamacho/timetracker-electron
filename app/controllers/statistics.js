angular.module('workTimeTrackerApp').controller('StatisticsCtrl', ['$scope', 'activities', function ($scope, activities) {
  $scope.activities = activities.getAll();
  $scope.today = new Date();

  // Saving data on localStorage

  //localStorage.setItem("activities", JSON.stringify($scope.activities));
  //console.log(localStorage.getItem("activities"));
  
	fs.writeFile(__dirname +'/test.json', JSON.stringify($scope.activities), function(err) {
	    if(err) {
	        return console.log(err);
	    }

	    console.log("The file was saved!");
	});
}]);
