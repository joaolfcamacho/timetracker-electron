var app = require('app');  // Module to control application life.
var BrowserWindow = require('browser-window');  // Module to create native browser window.

// Report crashes to our server.
require('crash-reporter').start();

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the javascript object is GCed.
var mainWindow = null;

// Quit when all windows are closed.
app.on('window-all-closed', function() {
  if (process.platform != 'darwin')
    app.quit();
});

// This method will be called when Electron has done everything
// initialization and ready for creating browser windows.
app.on('ready', function() {
  // Create the browser window.
  mainWindow = new BrowserWindow({width: 1200, height: 1000});

  // and load the index.html of the app.
  mainWindow.loadUrl('file://' + __dirname + '/index.html');

  mainWindow.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', function() {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
   /*
    // Saving data on localStorage
    //angular.element(html).scope();
    var ipc = require('ipc');
    
    //window.webContents.on('did-finish-load', function() {
      mainWindow.webContents.send('ping', 'whoooooooh!');
    //  });
  
    localStorage.setItem("activities", JSON.stringify($scope.activities));
    console.log(localStorage.getItem("activities"));
  
    //fs.writeFileSync('./data.json', window.MY_SCOPE.join(',') , 'utf-8'); 
    //JSON.stringify(obj)
    */
    mainWindow = null;
  });
});